{-# LANGUAGE BangPatterns #-}
-- Using Strict Data Types
import Data.List

data Pair a b = Pair !a !b
                     deriving Show

m = Pair 0 0 

g = foldl' (\(Pair m n) x -> (Pair (m+x) 0)) m [1..200000000]

