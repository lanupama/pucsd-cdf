take1 0 l = []
take1 n l = head l :take1 (n - 1) (tail l)

length1 [] = 0
length1 (_:l) = 1 + length1 l

--let v = error "undefined"
-- length1 (take 3 v)

--let g = undefined

--What is a thunk?
--A thunk is a value that is yet to be evaluated

{-
foldl (+) 0 [1, 2, 3, 4, 5, 6]
 = foldl (+) (0 + 1) [2, 3, 4, 5, 6]
 = foldl (+) ((0 + 1) + 2) [3, 4, 5, 6]
 = foldl (+) (((0 + 1) + 2) + 3) [4, 5, 6]
 = foldl (+) ((((0 + 1) + 2) + 3) + 4) [5, 6]
 = foldl (+) (((((0 + 1) + 2) + 3) + 4) + 5) [6]
 = foldl (+) ((((((0 + 1) + 2) + 3) + 4) + 5) + 6) []
 = (((((0 + 1) + 2) + 3) + 4) + 5) + 6
 = ((((1 + 2) + 3) + 4) + 5) + 6
 = (((3 + 3) + 4) + 5) + 6
 = ((6 + 4) + 5) + 6
 = (10 + 5) + 6
 = 15 + 6
 = 21
-}

-- try foldl (+) 0 [1..200000000]
-- try foldl' (+) 0 [1..200000000]
-- foldl/foldl' (\(m,n) y -> (m + y, 0)) (0,0) [1..200000000]
-- foldl' (\(!m,!n) y -> (m +y,0)) (0,0) [1..200000000]
--BangPatterns
