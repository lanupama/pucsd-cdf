type Error = String

fac :: Int -> Either Error Int
fac 0 = return 1 
fac n 
        |n > 0     = fac(n - 1) >>= \r -> return (n * r)
        |otherwise = Left "fac: negative argument"