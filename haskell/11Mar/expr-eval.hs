import qualified Data.Map as Map
import Data.Maybe 
import Control.Monad.State
import Control.Monad.Writer

type Env   = Map.Map Char Int
type Error = String

data AExp = Var Char
            | Value Int
            | Plus AExp AExp
            | Mod AExp AExp
            | Mul AExp AExp

aEval :: AExp -> Maybe Int
aEval (Var k)          =  Map.lookup k currEnv 
aEval (Value v)        =  return v
aEval (Plus ax1 ax2)   = (aEval ax1) 
                         >>=
                            (\x -> (aEval ax2) >>= \y -> return(x + y))
aEval (Mul ax1 ax2)    = do
                            x <- aEval ax1
                            y <- aEval ax2
                            return (x * y)

aEval1 :: AExp -> Either Error Int 
aEval1 (Var k)          =  case (Map.lookup k currEnv) of
                               Nothing -> Left $ "unbound variable " ++ [k]
                               Just a  -> return a 
aEval1 (Value v)        =  return v
aEval1 (Plus ax1 ax2)   = (aEval1 ax1) 
                         >>=
                            (\x -> (aEval1 ax2) >>= \y -> return (x + y))
aEval1 (Mul ax1 ax2)    = do
                            x <- aEval1 ax1
                            y <- aEval1 ax2
                            return (x * y)


--aEval (Mod ax1 ax2)    = (aEval ax1) `mod` (aEval ax2)

currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]

--runState (aEval2 (Plus (Var 'X') (Var 'Y'))) 0

aEval2 :: AExp -> State Int Int
aEval2 (Value v)        =  get >>= \x -> put (x+1) >> return v
aEval2 (Var k)          =  get >>= 
                               \x -> put(x+1) >>
                                               (return . fromJust . Map.lookup k $ currEnv)
aEval2 (Plus ax1 ax2)   =  do
  --                           s <- get
    --                         put (s + 1)
                             modify (+1) 
                             val1 <- aEval2 ax1
                             val2 <- aEval2 ax2
                             return (val1 + val2) 


{- newtype Writer w a = Writer {runWriter :: (a,w)}
           instance (Monoid w) => Monad (Writer w) where  
                    return x = Writer (x, mempty)  
                    (Writer (x,v)) >>= f = let (Writer (y, v')) = f x in Writer (y, v `mappend` v')  -}

aEval3 :: AExp -> Writer [String] Int  
aEval3 (Value v) =  writer (v, ["Evaluated a value"])
aEval3 (Var k)   =  do 
                       let v = fromJust . Map.lookup k $ currEnv 
                       writer (v, ["Evaluated a Variable " ++ show k])
aEval3 (Plus ax1 ax2) = do
                          val1 <- aEval3 ax1
                          val2 <- aEval3 ax2
                          writer (val1 + val2, ["Evaluated a Plus"]) 

--runWriter $ aEval3 (Plus (Var 'X') (Var 'Y'))