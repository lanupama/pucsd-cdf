> {-# LANGUAGE UnicodeSyntax #-}
> (∈) = elem

> ε = 4


-- हरि ॐ

 type Dfa q σ = (q → σ → [q], q, [q])


> type Δ  q σ = q → σ → q 
> type Q₀ q σ = q 
> type F  q σ = [q]


> type Dfa q σ = (Δ q σ, Q₀ q σ, F q σ)


> data St = One | Zero
>     deriving Eq

> δ One '1' = One
> δ One '0' = Zero
> δ Zero '0' = Zero
> δ Zero '1' = One
> st = One
> final = [Zero]


 accept :: [Char] → Bool
 accept [] cst =  cst
 accept (c:cs) cst = accept cs (cst `δ` c)

> accept s = (foldl δ st s) ∈ final
> data Stt = Zero1| One1|Two1|Three1
>     deriving Eq

> δ₁ Zero1 _ = One1
> δ₁ One1 _ = Two1
> δ₁ Two1 _ = Three1
> δ₁ Three1 _ = One1 

> accg (δ, st, final) s = (foldl δ st s) ∈ final

