-- Simple Program to remove comments
import Control.Monad

data State = InSingleLineComm
             |InMultiLineComm
             |InCode
             |InString
             
             |MaybeInSingleLineComm
             |MaybeInMultiLineComm


stripComm :: State -> String -> String

stripComm InCode []              = []
stripComm InCode ('"':xs)        = '"' : stripComm InString xs
stripComm InCode ('-':'-':xs)    = stripComm InCode (dropWhile (/= '\n') xs)
stripComm InCode ('{':'-':xs)    = stripComm InMultiLineComm xs
stripComm InCode (x:xs)          = x : stripComm InCode xs 


stripComm InString ('"':xs)      = '"' : stripComm InCode xs 
stripComm InString (x:xs)        = x : stripComm InString xs

stripComm InMultiLineComm ('-':'}':xs) 
                                 = stripComm InCode xs
stripComm InMultiLineComm (x:xs) = stripComm InMultiLineComm xs 


main = getContents >>= putStrLn . (stripComm InCode) 


--second version
stripComm2 :: (String,State) -> Char -> (String,State)
stripComm2 (xss,InCode) '-' = (xss,MaybeInSingleLineComm)
stripComm2 (xss,InCode)  c  = (c:xss,InCode)
stripComm2 (xss, MaybeInSingleLineComm) '-'
                            = (xss, InSingleLineComm)
stripComm2 (xss, MaybeInSingleLineComm) c
                            = (c:'-':xss, InCode)
stripComm2 (xss, InSingleLineComm) '\n'
                            = ('\n': xss,InCode)
stripComm2 (xss, InSingleLineComm) _
                            = (xss, InSingleLineComm) 

-- Monadic version


stripCommM :: State -> Char -> IO State
stripCommM InCode '-'      = return MaybeInSingleLineComm 
stripCommM InCode x        = putChar x  >> return InCode
stripCommM MaybeInSingleLineComm '-' 
                           = return InSingleLineComm
stripCommM MaybeInSingleLineComm x
                           = putChar '-' >> putChar x >> return InCode 
stripCommM InSingleLineComm '\n' 
                           = putChar '\n' >> return InCode
stripCommM InSingleLineComm x
                           = return InSingleLineComm 


main2 = getContents >>= foldM_ stripCommM InCode 



--dummyString = "ssds------lklklk{-jhjhj-}" 

--Sample MultiLine Comm 

--dummyString1 = "ssds------lklklk{-jhjhj-}" 




