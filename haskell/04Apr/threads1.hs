import Control.Concurrent

main1 = do
        m <- newEmptyMVar
        forkIO $ do
                   v <- takeMVar m
                   putStrLn ("received" ++ show v)
        putStrLn "sending"
        putMVar m "wake up"
        threadDelay (10^6 * 6)

--newMVar

main01 = do
       m <- newEmptyMVar
       forkIO $ putMVar m "Second thread put"
--       r <- takeMVar m
--       putStrLn r
       threadDelay (10^6 * 6)
       putMVar m "First thread put"
       r <- takeMVar m
       putStrLn r

main3 = do
      m <- newEmptyMVar
      takeMVar m


main = do
        m <- newEmptyMVar
        putMVar m "First put by main thread"
        takeMVar m
        putMVar m "Second put by main thread"
        r <- takeMVar m
        putStrLn r 

main7 = do
        m <- newEmptyMVar
        putMVar m "put MVar value"
        r <- takeMVar m
        putStrLn "read Mvar value"
        putStrLn $ r
        



--BlockedIndefinitelyOnMVar
