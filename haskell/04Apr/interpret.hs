import qualified Data.Map as Map
import Data.Maybe 

type Env   = Map.Map Char Int

data BoolExp = BTrue
               | BFalse
               | Le  ArithExp ArithExp
               | Gr  ArithExp ArithExp
               | Ee  ArithExp ArithExp
               | And BoolExp BoolExp
                

data ArithExp =  Var Char
                |Val Int
                |Plus ArithExp ArithExp
                |Mul ArithExp ArithExp

data Statement = Assign Char ArithExp   
                 | While BoolExp [Statement]

type Source = [Statement]

aEval :: ArithExp -> Env -> Int
aEval (Var c) currEnv           = fromJust $ Map.lookup c currEnv 
aEval (Val v) _                 = v
aEval (Plus ax1 ax2) currEnv    = appAop (+) ax1  ax2 currEnv 
aEval (Mul ax1 ax2) currEnv     = appAop (*) ax1  ax2 currEnv


appAop :: (Int -> Int -> Int) -> ArithExp -> ArithExp -> Env ->  Int
appAop op ax1 ax2 currEnv = op (aEval ax1 currEnv) (aEval ax2 currEnv)


bEval :: BoolExp -> Env -> Bool
bEval BTrue  _                  = True
bEval BFalse _                  = False
bEval (Le ax1 ax2) currEnv      = appBop (<) ax1 ax2 currEnv
bEval (Gr ax1 ax2) currEnv      = appBop (>) ax1 ax2 currEnv
bEval (Ee ax1 ax2) currEnv      = appBop (==) ax1 ax2 currEnv
bEval (And bx1 bx2) currEnv     = bEval bx1 currEnv && bEval bx2 currEnv


appBop :: (Int -> Int -> Bool) -> ArithExp -> ArithExp -> Env -> Bool
appBop op ax1 ax2 currEnv = op (aEval ax1 currEnv) (aEval ax2 currEnv) 


interpret :: Env -> Statement -> Env
interpret currEnv (Assign c ax1)   = modifyEnv (c,(aEval ax1 currEnv)) currEnv
interpret currEnv w@(While bx1 sts)  = 
                 if (bEval bx1 currEnv) then  
                     interpret (foldl interpret currEnv sts) w --(While bx1 sts)
                 else
                     currEnv

modifyEnv :: (Char,Int) -> Env -> Env
modifyEnv (k,val) currEnv = Map.alter (const (Just val)) k currEnv

main1 :: Source -> Env -> Env
main1 scs currEnv = foldl interpret currEnv scs


-- Test Data
source = [Assign 'A' (Val 2), 
          Assign 'B' (Var 'Z'),
          While (Le (Var 'Z') (Var 'Y')) [Assign 'Z' (Plus (Var 'Z') (Val 1))]
--          While (Ee (Var 'Z') (Var 'Y')) [While BFalse [],
--                                          Assign 'Y' (Plus (Var 'Y') (Val 1)) ],
--          Assign 'Z' (Val 0),
--          While (And (Le (Var 'Z') (Var 'A')) BTrue) 
--                                         [Assign 'Z' (Plus (Var 'Z') (Val 1))] 
          ]
currEnv = Map.fromList[('X',2),('Y',3),('Z',0)] :: Map.Map Char Int