import Control.Concurrent
import Control.Monad
import System.IO

main = do
       hSetBuffering stdout NoBuffering
       x <- forkIO (getLine >> return ())
--       forkIO (replicateM_ 100 (putChar 'B'))
       putStrLn $ show x
--       replicateM_ 100 (putChar 'A')

