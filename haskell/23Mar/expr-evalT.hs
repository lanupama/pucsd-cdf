import qualified Data.Map as Map
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Class
import Control.Monad.State


type Env   = Map.Map Char Int
type Error = String

data AExp = Var Char
            | Value Int
            | Plus AExp AExp
            | Mod AExp AExp
            | Mul AExp AExp

-- run as runStateT (aEval2 <exp>) 0
-- Note the Difference between the aEval7 from exp-eval-new.hs - in the return
-- types as well as in the case analysis

aEval2 :: AExp -> StateT Int Maybe Int
aEval2 (Value v)        =  do 
                              x <- get 
                              put $ (x+1) 
                              return $ v
aEval2 (Var k)          =  get 
                           >>= 
                              \x -> put(x+1) 
                              >>
                                lift (Map.lookup k $ currEnv)

aEval2 (Plus ax1 ax2)   =  do
                             s <- get
                             put (s + 1)
    --                         modify (+1) 
                             val1 <- aEval2 ax1
                             val2 <- aEval2 ax2
                             return $ (val1 + val2)  


currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]