import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Class
import Control.Monad

type Password = String

isValid :: Password ->  MaybeT IO Password
isValid p = MaybeT . return $ if (length p) > 6 then Just p else Nothing  


-- run main1 as runMaybeT main1

main1 :: MaybeT IO () 
main1 = do
          x <- lift getLine

--          x <- MaybeT . (liftM Just) $ getLine
          isValid x 
          lift $ putStrLn x 

-- main2 wont compile? Can you Identify why?. Can you fix this before you 
-- proceed to main3
{-
main2 :: IO ()
main2 = do
           x <- getLine
           y <- if (length x) > 6 then Just x else Nothing )
              
           putStrLn y  -}

-- Just run main3. There are 3 ways of writing main3
-- Can you guess what the Inner do in the uncommented approach is?

main3 :: IO ()
main3 = do
           x <- getLine
--2           let z = if (length x) > 6 then Just x else Nothing
--2                   >>=
--2                      \x -> Just ("hello" ++ x)    

--1        z <- return (main4 x)

           let z = do
                      y <- if (length x) > 6 then Just x else Nothing
                      Just ("hello" ++ y)    
                   
           putStrLn $
                        case z of
                             Nothing  -> "Nothing"
                             (Just a) -> a 

main4 :: String -> Maybe String
main4 x  = if (length x) > 6 then Just x else Nothing
           >>=
                \x -> Just ("hello" ++ x)  
                