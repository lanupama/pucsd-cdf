import qualified Data.Map as Map
import Data.Maybe 
import Control.Monad.State
import Control.Applicative


type Env   = Map.Map Char Int
type Error = String

data AExp = Var Char
            | Value Int
            | Plus AExp AExp
            | Mod AExp AExp
            | Mul AExp AExp

aEval7 :: AExp -> State Int (Maybe Int)
aEval7 (Value v)        =  modify(+1) >> return (Just v)
aEval7 (Var k)          = let y = Map.lookup k $ currEnv in 
                                      (case y of
                                           Nothing -> put 0    
                                           _       -> modify(+1) ) >> return y
aEval7 (Plus aex1 aex2) = do
                            val1 <- aEval7 aex1
                            val2 <- aEval7 aex2
                         -- let y = fmap (+) val1 <*> val2  
                            let y = liftM2 (+) val1 val2 
                            case y of
                               Nothing -> put 0
                               _       -> modify(+1)
                            return y
                           
currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]


