anupama@anupama-desktop:~/bitbucket-repos/pucsd-cdf/haskell/21Mar$ ghci
Prelude Control.Monad.Trans.Maybe> :t mapM
mapM :: Monad m => (a -> m b) -> [a] -> m [b]
Prelude Control.Monad.Trans.Maybe> :t seq
seq :: a -> b -> b
Prelude Control.Monad.Trans.Maybe> :m Control.Monad
Prelude Control.Monad> liftM (\x -> [x]) [1,2,3]
[[1],[2],[3]]
Prelude Control.Monad> fmap (\x -> [x]) [1,2,3]
[[1],[2],[3]]
Prelude Control.Monad> :t fmap
fmap :: Functor f => (a -> b) -> f a -> f b
Prelude Control.Monad> mapM (\x -> [x]) [1,2,3]
[[1,2,3]]
Prelude Control.Monad> :t mapM
mapM :: Monad m => (a -> m b) -> [a] -> m [b]
Prelude Control.Monad> mapM_ (\x -> [x]) [1,2,3]
[()]
Prelude Control.Monad> fmap (\x -> x) [1,2,3]
[1,2,3]
Prelude Control.Monad> mapM putStrLn ["1","2","3"]
1
2
3
[(),(),()]
Prelude Control.Monad> :t mapM putStrLn ["1","2","3"]
mapM putStrLn ["1","2","3"] :: IO [()]
Prelude Control.Monad> :t mapM_ putStrLn ["1","2","3"]
mapM_ putStrLn ["1","2","3"] :: IO ()
Prelude Control.Monad> mapM_ putStrLn ["1","2","3"]
1
2
3
Prelude Control.Monad> liftM2 (+) (Just 2) (Just 3)
Just 5
Prelude Control.Monad> mapM Just [1,2,3]
Just [1,2,3]
Prelude Control.Monad> :t (:)
(:) :: a -> [a] -> [a]
Prelude Control.Monad> liftM2 (:) (Just 1) (Just [2])
Just [1,2]
Prelude Control.Monad> :t sequence
sequence :: Monad m => [m a] -> m [a]
Prelude Control.Monad> liftM2 (+) (Just 2) (Just 3)
Just 5
Prelude Control.Monad> sequence [Just 1, Just 2] 
Just [1,2]
Prelude Control.Monad> sequence [putStrLn "HI"]
HI
[()]
Prelude Control.Monad
