data Tree a = Leaf a
              | Node a (Tree a) (Tree a)


sumTC (Leaf x) k = k x
sumTC (Node a left right) k = sumTC left contleft
    where contleft sleft = sumTC right contright
              where contright sright = k (a + sleft + sright)

sumTC2 (Leaf x) k = k x
sumTC2 (Node a left right) k = 
       sumTC2 left (\sleft -> sumTC2 right (\sright -> k (a + sleft + sright)))

{-

sumTC2 (Node 1 (Node 2 (Leaf 1) (Leaf 2)) (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)))  id

sumTC2 (Node 2 (Leaf 1) (Leaf 2)) (\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright)))

sumTC2 (Leaf 1) (\sleft -> sumTC2 (Leaf 2)(\sright -> ((\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright))) (2 + sleft + sright))))

(\sleft -> sumTC2 (Leaf 2)(\sright -> ((\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright))) (2 + sleft + sright)))) 1

sumTC2 (Leaf 2)(\sright -> ((\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright))) (2 + 1 + sright)))

(\sright -> ((\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright))) (2 + 1 + sright))) 2

((\sleft -> sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + sleft + sright)))  (2 + 1 + 2)) 

 sumTC2 (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)) (\sright -> id (1 + (2 + 1 + 2) + sright))  

 sumTC2 (Node 0 (Leaf 4) (Leaf 5)) (\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright)))

 sumTC2 (Leaf 4)  (\sleft -> sumTC2 (Leaf 5) (\sright -> (\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright))) (0 + sleft + sright)))

(\sleft -> sumTC2 (Leaf 5) (\sright -> (\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright))) (0 + sleft + sright))) 4

sumTC2 (Leaf 5) (\sright -> (\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright))) (0 + 4 + sright))

(\sright -> (\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright))) (0 + 4 + sright)) 5

(\sleft -> sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 +sleft +sright))) (0 + 4 + 5)

sumTC2 (Leaf 6) (\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 + (0 + 4 + 5) +sright))

(\sright -> (\sright -> id (1 + (2 + 1 + 2) + sright)) (3 + (0 + 4 + 5) +sright)) 6

(\sright -> id (1 + (2 + 1 + 2) + sright)) (3 + (0 + 4 + 5) + 6)

id (1 + (2 + 1 + 2) + (3 + (0 + 4 + 5) + 6)) 

(1 + (2 + 1 + 2) + (3 + (0 + 4 + 5) + 6)) 

(Node 1 (Node 2 (Leaf 1) (Leaf 2)) (Node 3 (Node 0 (Leaf 4) (Leaf 5)) (Leaf 6)))

-}

