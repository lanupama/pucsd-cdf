data Tree a = Empty | Leaf a | Node (Tree a) (Tree a) deriving Show

--problem : To give the leftmost element and the remainder of the Tree

-- Simple Recursive Defn
splitLeft :: Tree a -> (a, Maybe (Tree a))
splitLeft (Leaf a) = (a, Nothing)
splitLeft (Node l r) = case splitLeft l of
                            (a,Nothing) ->  (a, Just r)
                            (a, Just l1) -> (a, Just (Node l1 r))
                 

-- CPS Style 

splitLeftC (Leaf a) k    = k (a, Nothing)
splitLeftC (Node l r) k  = splitLeftC l $ \m -> case m of
                                                    (a, Nothing)  -> k (a, Just r)
                                                    (a, Just l1)  -> k (a, Just (Node l1 r))
{-

splitLeftC (Node (Node (Leaf 5) (Leaf 6)) (Node (Leaf 1) (Leaf 2))) id
splitLeftC (Node (Leaf 5) (Leaf 6)) $ \m -> case m of
                                                    (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                    (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))
splitLeftC (Leaf 5) $ \m -> case m of
                                                    (a, Nothing)  -> (\m -> case m of
                                                                        (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                                        (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))) (a, Just (Leaf 6))
                                                    (a, Just l1)  ->  (\m -> case m of
                                                                        (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                                        (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))) (a, Just (Node l1 (Leaf 6))
(\m -> case m of
                                                    (a, Nothing)  -> (\m -> case m of
                                                                        (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                                        (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))) (a, Just (Leaf 6))
                                                    (a, Just l1)  ->  (\m -> case m of
                                                                        (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                                        (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))) (a, Just (Node l1 (Leaf 6)))) (5,Nothing)

(\m -> case m of
                                                                        (a, Nothing)  -> id (a, Just (Node (Leaf 1) (Leaf 2)))
                                                                        (a, Just l1)  -> id (a, Just (Node l1 (Node (Leaf 1) (Leaf 2))))) (5, Just (Leaf 6))

id (5, Just (Node (Leaf 6) (Node (Leaf 1) (Leaf 2))))
(5, Just (Node (Leaf 6) (Node (Leaf 1) (Leaf 2))))
-}


{-
splitLeft (Node (Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))) 
splitLeft (Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6))

splitLeft (Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6))

splitLeft (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5))

splitLeft (Node (Leaf 3) (Leaf 4)) 

(3, Just (Leaf 4))

(3, Just (Node (Leaf 4) (Leaf 5)))

(3, Just (Node (Leaf 4) (Leaf 5)))

(3, Just (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)))

(3, Just (Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))) ) -}

--- Node (Leaf 1) (Leaf 2)
--- Node (Leaf 6) (Node (Leaf 1) (Leaf 2))
--- (Node (Node (Leaf 5) (Leaf 6)) (Node (Leaf 1) (Leaf 2)))
--- (Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2)))

--First Approach
--Strictly this is the equivalent of the recursive call. It is a good
-- exercise to understand how the recursion unfolds. (Nitin I think u used a similar approach in his attempt)


splitLeftT1 :: Tree a -> [Tree a] -> (a,Maybe (Tree a))
splitLeftT1 (Leaf a) aux    = (a, foldl construct Nothing aux)
splitLeftT1 (Node m b) aux  = splitLeftT1 m (b:aux)

construct :: Maybe (Tree a) -> Tree a -> Maybe (Tree a)
construct Nothing m      = Just m
construct (Just m) n     = Just (Node m n) 


-- Second approach
--The Example below shows how CPS style is the preferred way to go about
-- to make the above function tail-recursive in the splitLeft case. This example has only been provided
-- for completeness sake and help to understand how the recursive version of the code works.

splitLeftT :: Tree a -> Maybe (Tree a) -> (a, Maybe (Tree a))
splitLeftT (Leaf a)   aux      =  (a,aux)
splitLeftT (Node (Leaf a) n) Nothing
                               = (a, Just n)
splitLeftT (Node p q) Nothing     
                               = splitLeftT p (Just (Node Empty q))
splitLeftT (Node (Leaf a) q) (Just m)    
                               = (a, Just (mkTree Final q m))         
splitLeftT (Node p q) (Just m)          
                               = splitLeftT p $ Just (mkTree Inter q m)

-- auxillary function 'mkTree' for reconstructing intermediate tree, This function is analogous to (++) used in makeListT
-- auxillary type for the above 'Stage'  
-- Note the extension of the Tree type to support Empty Tree, This is only used to for the intermediate tree
-- whose left branch is yet to be constructed and will be in the next call.

data Stage = Inter | Final
                             
mkTree :: Stage -> Tree a -> Tree a -> Tree a 

mkTree Inter q (Node Empty m) = Node (Node Empty q) m
mkTree Inter q (Node p m)     = Node (mkTree Inter q p) m
mkTree Final q (Node Empty m) = Node q m  
mkTree Final q (Node p m)     = Node (mkTree Final q p) m  

{-
splitLeftT (Node (Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2)))  Nothing
splitLeftT (Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6)) (Just (Node Empty (Node (Leaf 1) (Leaf 2))))
splitLeftT Node (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Leaf 6)) (Just (Node Empty (Node (Leaf 1) (Leaf 2))))
splitLeftT (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Just (mkTree Inter (Leaf 6)(Node Empty (Node (Leaf 1) (Leaf 2)))))
.....(mkTree Inter (Leaf 6)(Node Empty (Node (Leaf 1) (Leaf 2))))
..... Node (Node Empty (Leaf 6)) (Node (Leaf 1) (Leaf 2))
splitLeftT (Node (Node (Leaf 3) (Leaf 4)) (Leaf 5)) (Just $ Node (Node Empty (Leaf 6)) (Node (Leaf 1) (Leaf 2)))
splitLeftT (Node (Leaf 3) (Leaf 4)) (Just $ mkTree Inter (Leaf 5) (Node (Node Empty (Leaf 6)) (Node (Leaf 1) (Leaf 2))))
........mkTree Inter (Leaf 5) (Node (Node Empty (Leaf 6)) (Node (Leaf 1) (Leaf 2)))
....... Node (mkTree Inter (Leaf 5) (Node Empty (Leaf 6))) (Node (Leaf 1) (Leaf 2))
....... Node (Node (Node Empty (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))
.......
splitLeftT (Node (Leaf 3) (Leaf 4)) $ Node (Node (Node Empty (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))
splitLeftT (Leaf 3) splitLeftT p $ Just (mkTree Final (Leaf 4) (Node (Node (Node Empty (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))))
........mkTree Final (Leaf 4) (Node (Node (Node Empty (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2)))
........Node (mkTree Final (Leaf 4) (Node (Node Empty (Leaf 5)) (Leaf 6))) (Node (Leaf 1) (Leaf 2))
........Node (Node (mkTree Final (Leaf 4) (Node Empty (Leaf 5))) (Leaf 6)) (Node (Leaf 1) (Leaf 2))
........Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))
splitLeftT (Leaf 3) (Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2)))
(3, (Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Leaf 1) (Leaf 2))))
-}

--Third approach
--Extend the last example to the more familiar type. For this you will have to pass
--a suitable a for the second parameter , eg- calling for a::Int, , you could use
--(0,Nothing). The first element of the tuple will anyway be discarded   
-- It may optionally have  a 
--splitLeftT Empty      aux           = aux
-- In either case you may need to extend the defn of mkTree. The completion is
--left as an exercise 


{-
splitLeftT :: Tree a -> (a,Maybe (Tree a)) -> (a, Maybe (Tree a)) 

splitLeftT (Leaf a)   (m,Just tr)   = (a,Just (mkTree Final Empty tr))
splitLeftT (Leaf a)   (m,Nothing)   = (a,Nothing) -- splitLeftT Empty (a,Nothing)
splitLeftT (Node p q) (m,Nothing)   = splitLeftT p (m,Just (Node Empty q))
splitLeftT (Node p q) (Just m)      = splitLeftT p (m,Just (mkTree Inter q m))
-}


