sum1 []         = 0
sum1 (x:xs)     = 1 + sum1 xs

sumT []     acc = acc
sumT (x:xs) acc = sumT xs (acc+1)

makeList 0 = []
makeList n = n : makeList (n-1)

makeListT 0 l = l
makeListT n l = makeListT (n-1) (n : l) --(l++[n]) (Note that (:) gives list in reverse order

makeListC 0 k = k []
makeListC n k = makeListC (n-1) $ \l -> k (n : l)


sumC [] k     = k 0
sumC (x:xs) k = sumC xs $ \m -> k (x + m)


lengthC [] k     = k 0
lengthC (x:xs) k = lengthC xs $ \m -> k (1 + m)


--- Simple Function

add x y k = \k -> k ( x + y)

fact1 0 = 1
fact1 n = n * fact1 (n - 1)

factT 0 acc = acc
factT n acc = factT (n-1) (acc * n)

factC 0 k = k 1
factC n k = factC (n - 1) $ \m -> k ( n * m)


-- Example use with a higher order function map

map1 f []       = []
map1 f (x:xs)   = f x : map1 f xs


mapT0 f [] k = k []
mapT0 f (x:xs) k
--              = (mapT0 f xs) (\p -> k (f x : p))
              = f x (\q -> (mapT0 f xs) (\p -> k (q : p))) -- mapC


{--
   mapT0 (\ m -> (k -> k (1 + m))) [2,4,6]  id   -- same as (\m k -> k ( 1+m))
   (\ m -> (\k -> k (1 + m))) 2 ....  (Function application is left associative)
   (\k -> k (1 + 2)) (\q -> (mapT0 f [4,6]) (\p -> id (q : p)))
   (\q -> (mapT0 f [4,6]) (\p -> id (q : p))) (1 + 2)
   (mapT0 f [4,6]) (\p -> id ((1+2) : p))
   (\ m -> (\k -> k (1 + m))) 4 ......
   (\k -> k (1 + 4)) (\q -> (mapT0 f [6]) (\p -> (\p -> id ((1+2) : p)) (q : p))) 
   (\q -> (mapT0 f [6]) (\p -> (\p -> id ((1+2) : p)) (q : p))) (1+4)
   mapT0 f [6] (\p -> (\p -> id ((1+2) : p)) ((1+4):p))
   (\ m -> (\k -> k (1 + m))) 6 ....
   (\k -> k (1 + 6)) (\q -> (mapT0 f []) (\p -> (\p -> (\p -> id ((1+2) : p)) ((1+4):p)) (q : p)))
   (\q -> (mapT0 f []) (\p -> (\p -> (\p -> id ((1+2) : p)) ((1+4):p)) (q : p))) (1+6)
   (mapT0 f []) (\p -> (\p -> (\p -> id ((1+2) : p)) ((1+4):p)) ((1+6) : p)) 
   (\p -> (\p -> (\p -> id ((1+2) : p)) ((1+4):p)) ((1+6) : p)) []
   (\p -> (\p -> id ((1+2) : p)) ((1+4):p)) ((1+6) : []) 
   (\p -> id ((1+2) : p)) ((1+4):((1+6) : []))
    id ((1+2) : ((1+4):((1+6) : [])))
   ((1+2) : ((1+4):((1+6) : [])))
   [1+2 ,1+4 ,1+5]

-}


{-
What is CPS?
 (a->r) -> r is a suspended computation which when given a function another
function as an argument, produces a final result
(a->r) is the continuation and specifies how the computation will be
brought to a conclusion
-}

