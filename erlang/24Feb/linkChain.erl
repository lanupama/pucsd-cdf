-module(linkChain).
-export([chain/1, chain1/1]).

chain1(0) ->
    receive 
    _  		->  ok
    after 5000	-> exit("chain dies here")
    end;
chain1(N) ->
     Pid = spawn(fun() -> io:format("spawning ~w~n",[N]),chain1(N-1) end),
     io:format("My N:~w,Pid:~w My child pid:~w~n", [N, self(), Pid]),
%     Pid = spawn(?MODULE,chain,[N-1]),
     link(Pid),
     process_flag(trap_exit,true),
     receive
         {'EXIT',P,REASON} -> io:format("received Reason~s N:~w in Pid:~w from Pid:~w~n",[REASON,N,self(),P]), ok
     end.


chain(0) ->
    receive 
    _  		->  ok
    after 5000	-> exit("chain dies here")
    end;
chain(N) ->
     Pid = spawn(fun() -> io:format("spawning ~w~n",[N]),chain(N-1) end),
     io:format("My N:~w,Pid:~w My child pid:~w~n", [N, self(), Pid]),
%     Pid = spawn(?MODULE,chain,[N-1]),
     link(Pid),
     receive
         _ -> ok
     end.
    

%% erlang:monitor(process,spawn(fun() -> timer:sleep(500) end)).
%% {Down,MonitorRef,Type,Object,Info}
