-module(perm).
-export([take/2,aint/2,perm/1]).

take(N,L) -> {F,R} = lists:split(N,L),
    	     F.

drop(N,L) -> {F,R} = lists:split(N,L),
    	     R.

perm([])     ->  [[]];
perm([X|XS]) -> [Y || P <- perm(XS), Y <- aint(X,P)].

aint(X,L) -> [take(I,L) ++ [X] ++ drop(I,L) || I <- lists:seq(0,length(L))] .


    

    
