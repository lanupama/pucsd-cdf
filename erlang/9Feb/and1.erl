-module(and1).
-export([and1/1, and2/1, and3/1, curry/1, g/2]).

and1([]) -> true;
and1([X|Xs]) -> X and and1(Xs). 

and2(L)-> F = fun(X,Y)-> X and Y end,
	  lists:foldl(F,true, L).
    
and3(L) -> lists:foldl(fun (X,Y) -> X and Y end, true , L).

and4(L) -> lists:foldl(fun g/2,true,L).
     
g(X,Y) -> X and Y.     

curry(F) -> fun(X) -> (fun(Y) -> apply (F, [X ,Y]) end) end. 

curry1(F) -> fun(X) -> (fun(Y) -> F(X,Y) end) end.      
     
