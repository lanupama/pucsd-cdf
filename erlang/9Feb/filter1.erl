-module(filter1).
-export([filter/2,even1/1]).

filter(P,[])     -> [];
filter(P,[X|Xs]) ->
%%     case apply(P,[X]) of
       case P(X) of
	true  -> [X|filter(P,Xs)];
	_     -> filter(P,Xs)
     end.

even1(X) -> X rem 2 == 0.	    

%% filter1:filter(fun filter1:even1/1 ,[1,2,3]).     
		      
	      
