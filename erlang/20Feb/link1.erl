-module(link1).
-export([start/0,client_request/1,server_loop/0]).

start() ->
    process_flag(trap_exit, true),
    register(server1,spawn_link(link1,server_loop,[])).

client_request(X) -> 
    server1 ! {increment,self(),X},
    client_receive().

client_receive() ->
    Pid = whereis(server1),
    receive
%%        {'EXIT',Pid,Reason}  ->  {Pid,Reason};
%%        {'EXIT',Pid1,Reason1} -> {normal_exit,Pid1}, client_receive();
        ReturnVal -> ReturnVal
    end.
    
	    
server_loop() ->
    receive 
        {increment,Sender,Val} -> Sender ! Val + 1
    end,
    server_loop().	     

%% process_flag(trap_exit, true).
%%erlang:monitor(process,Proc) Proc can be a pid or a registered nam
%%erlang:demonitor(Reference)


