-module(receive1).
-export([recv1/0,recv2/0,priority1/0]). 

%% suspends or sleeps the current proces for T milli seconds
recv1() ->
    receive    
    after 2000 -> true
    end.

%% empties or flushes messages in the mailbox
recv2() ->
    receive
	_Any -> recv2()
    after 0 -> true
    end.

%% priority receive
priority1() ->
   receive
       {alarm,X} -> priority_alarm
   after 0 ->
	 receive
	     Any ->
		  Any
	 end
   end.
	    

	    
