-module(error2).
-export([fact/1, g/1, bind/0]).

fact(0) ->  1;
fact(N) -> if N > 0 -> N * fact(N-1)end.

g (H) -> try (fact(H)) of
            0 -> ok;
	    N when N>0 -> ok
         catch 
          error:X -> io:format("~w", [X]);
	  throw:X   ->throw;
	  exit:X    ->exit
	 end.	 
    
bind() ->      
    try (X=3) of
	Val -> normal
    catch
      _:_  -> exception 
    end.
	    
	    
